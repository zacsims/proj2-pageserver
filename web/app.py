from flask import Flask,request, render_template,abort

app = Flask(__name__)



@app.route("/")
@app.route('/<path:path>')
def trivia(path):
	print("path is:", path)
	if '//' in path or '/~' in path\
	or '/..' in path:
		abort(403)
	elif 'trivia.html' in path:
		return render_template('trivia.html')
	abort(404)


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
